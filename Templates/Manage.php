<?php

/**
 * @var $app \SiaasSlim
 * @var $managePage  string
 */


//Get the user
//We have already checked if someone is logged in
$user = $app->getLoggedInUser();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>SiaaS Manage</title>

    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <style>
        .header .user {
            text-align:right;
            padding-top:30px;
        }

        .sidebar {
            padding-top:22px;
        }

        .footer {
            margin-top:40px;
        }
    </style>

</head>
<body>

    <div class="container">

        <!-- Header -->
        <div class="row header">
            <div class="col-sm-6"><h1>SiaaS</h1></div>
            <div class="col-sm-6 user"><p><?php echo "Welcome, " . $user->getName(); ?></p></div>
        </div>

        <!-- Body -->
        <div class="row">
            <div class="col-sm-3 sidebar">
                <div class="list-group">
                    <a href="/manage" class="list-group-item"><span class="glyphicon glyphicon-home"></span> Home</a>
                </div>

                <div class="list-group">
                    <?php
                    //Format: <a href="/integration/[id]" class="list-group-item">[Integration Name]</a>
                    foreach($user->getUserIntegrations() as $userIntegration) {
//                        echo '<a href="/manage/integration/' . $userIntegration->getId() . '" class="list-group-item">' . $userIntegration->getName() . '</a>';
                        echo '<a href="/manage/integration/' . $userIntegration->getId() . '" class="list-group-item">Echo Bot</a>';
                    }
                    ?>
                    <a href="/manage/integration/create" class="list-group-item"><span class="glpyhicon glyphicon-plus"></span> New Integration</a>
                </div>

                <div class="list-group">
                    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-user"></span> Account</a>
                    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-question-sign"></span> Support</a>
                    <a href="#" class="list-group-item"><span class="glyphicon glyphicon-pencil"></span> Suggest Integration</a>
                </div>
            </div>

            <div class="col-sm-9">
                <?php
                //MAIN PAGE CONTENT

                if($managePage == 'home') {

                    //Home page
                    echo '<h2>Manage Home</h2>';
                    echo '<p>Select an integration from the left to manage or create a new one</p>';

                } elseif($managePage == 'editIntegration') {

                    //Manage an integration
                    /**
                     * @var $id string
                     */

                    //Load the integration
                    $userIntegration = new UserIntegration();
                    $userIntegration->fetch($id);

                    //Draw the page
                    echo '<form action="/manage/update" method="post">';

                        //ID
                        echo '<input type="hidden" name="id" value="' . $id . '" />';

                        //Name


                        //Fields

                        //Submit
                        echo '<button type="submit" class="btn btn-primary btn-lg">Save</button>';

                    echo '</form>';

                } elseif($managePage == 'createIntegration') {

                    //Create a new integration
                    //If they have picked an integration, $_GET['integrationId'] will be set
                    //Otherwise show them a picker


                    if($app->request()->params('integrationId')) {

                        //It's set, show a form for details
                        $integration = new \Models\Integration($app);
                        $integration->fetch($app->request()->params('integrationId'));

                        echo '<h2>New ' . $integration->getName() . '</h2>';

                        $integrationValues = $integration->getIntegrationValues();


                        echo '<form action="/manage/integration/create" method="post">';

                            //Intgration ID
                            echo '<input type="hidden" name="integrationId" value="' . $integration->getId() . '" />';

                            //Token
                            echo '<p><label>Slack Outgoing Webhook Token</label><br /><input type="text" name="token" /></p>';

                            //List each option
                            foreach($integrationValues as $integrationValue) {

                                //Don't show hidden ones
                                if($integrationValue->getHidden()) {
                                    echo '<input type="hidden" name="' . $integrationValue->getId() . '" value="' . $integrationValue->getDefault() . '" />';
                                    continue;
                                }

                                //Display It
                                echo '<p>';

                                echo '<label>' . $integrationValue->getName() . '</label>';
                                echo '<br />';
                                echo '<input type="text" name="' . $integrationValue->getId() . '" value="' . $integrationValue->getDefault() . '" />';

                                echo '</p>';

                            }

                            //Submit
                            echo '<button type="submit" class="btn btn-primary">Create</button>';

                        echo '</form>';

                    } else {

                        //It's not set, show a list of integrations to choose from
                        echo '<h2>Pick an Integration</h2>';

                        $query = $app->getDatabase()->query("SELECT `id` FROM `Integrations`");
                        while($row = $query->fetch_assoc()) {

                            //Create object
                            $integration = new \Models\Integration($app);
                            $integration->fetch($row['id']);

                            echo '<h3>' . $integration->getName() . '</h3>';
                            echo '<p>' . $integration->getDescription() . '</p>';
                            echo '<a class="btn btn-primary" href="/manage/integration/create?integrationId=' . $integration->getId() . '">Create</a>';

                        }

                    }


                }

                ?>

            </div>
        </div>

        <!-- Footer -->
        <div class="row">
            <div class="col-xs-12 footer">
                <p><small>Made in 2 days for UQCS Hackathon.<br />SiaaS is not created by, affiliated with, or supported by Slack Technologies, Inc.</small></p>
            </div>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
</body>
</html>
