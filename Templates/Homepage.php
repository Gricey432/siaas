<!DOCTYPE html>
<html lang="en">
<head>
    <title>SiaaS</title>

    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/static/css/cover.css">
</head>
<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">SiaaS</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="/login">Login</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">
                <h1 class="cover-heading">Slack in a Snap</h1>
                <p class="lead">Set up useful integrations for your slack team in less than 60 seconds.<br />No programming, registration or servers required.</p>
                <p class="lead">
                    <a href="/login" class="btn btn-lg btn-default btn-slack">Signup with Slack</a>
                </p>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Made in 2 days for UQCS Hackathon.<br />SiaaS is not created by, affiliated with, or supported by Slack Technologies, Inc.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
</body>
</html>