<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 11:36 AM
 */

/*
 * Routing system for SIAAS
 * Keep categories in files
 * Keep files alphabetical
 */

/**
 * @var $app \SiaasSlim
 */


use Models\User;



// Homepage
$app->get('/', function() use ($app) {

    require("Templates/Homepage.php");

});


// API
$app->group('/api', function() use ($app) {

    // Version 1
    $app->group('/v1', function() use ($app) {

        require "Routes/Api/v1.php";

    });

});


// Hook
$app->map('/hook', function() use ($app) {

    //Run the actual hooks

    //Check the token
    $token = $app->request()->params('token');
    if(!$token) {
        die('no token');
    }

    //Try to fetch an integration off the token
    $qry = $app->getDatabase()->query("SELECT `id` FROM `UserIntegrations` WHERE `slackToken` = '" . $app->getDatabase()->real_escape_string($token) . "'");

    if($qry->num_rows != 1) {
        die('bad token');
    }

    //Create the userIntegration
    $userIntegration = new \Models\UserIntegration($app);
    $userIntegration->fetch($qry->fetch_assoc()['id']);

    //Create the integration template
    $integration = new \Models\Integration($app);
    $integration->fetch($userIntegration->getIntegrationId());

    //Fetch the settings
    $userIntegrationValues = array();
    $qry = $app->getDatabase()->query("SELECT `id` FROM `UserIntegrationValues` WHERE `userIntegrationId` = " . $userIntegration->getId());
    while($row = $qry->fetch_assoc()) {
        $userIntegrationValue = new \Models\UserIntegrationValue($app);
        $userIntegrationValue->fetch($row['id']);
        array_push($userIntegrationValues, $userIntegrationValue);
    }

    //HookRequest
    $hookRequest = new \Models\HookRequest($app);

    //Integration Object
    $className = '\Integrations\\' . $integration->getPhpClass();
    $integrationObject = new $className;

    //Respond as json
    $app->view(new \JsonApiView());
    $app->add(new \JsonApiMiddleware());

    //Run
    $app->render(200,
        $integrationObject->run($app, $hookRequest, $userIntegrationValues)
    );


})->via('GET', 'POST');


// Login
$app->get('/login', function() use ($app) {

    //Handle OAUTH
    require "Routes/Login.php";

});


//Manage
$app->group('/manage', function() use ($app) {

    //Manage home page
    $app->get('/', function() use ($app) {

        //Ensure the user is logged in
        if(!$app->isLoggedIn()) {
            $app->redirect("http://128.199.101.170/");
        }

        $managePage = 'home';

        require("Templates/Manage.php");

    });

    //Manage view integration
    $app->get('/integraion/:id', function($id) use ($app) {

        //Ensure the user is logged in
        if(!$app->isLoggedIn()) {
            $app->redirect("http://128.199.101.170/");
        }

        $managePage = 'editIntegration';

        require("Templates/Manage.php");

    });

    //Manage create an integration
    $app->map('/integration/create', function() use ($app) {

        //Ensure the user is logged in
        if(!$app->isLoggedIn()) {
            $app->redirect("http://128.199.101.170/");
        }

        //Check if they're submitting a form or viewing one
        if($app->request()->isPost()) {

            //Submitting an integration
            //Save it
            die('save integration, not yet implemented');

        }

        //If they were successful they will have been redirected

        //Show the create form
        $managePage = 'createIntegration';

        require("Templates/Manage.php");

    })->via('GET', 'POST');


    //Manage update an integration
    $app->post('/integration/update', function() use ($app) {

        //Ensure the user is logged in
        if(!$app->isLoggedIn()) {
            $app->redirect("http://128.199.101.170/");
        }

        //Probably no time to make this, lets hope we get it right the first time
        die('Not implemented for this demo');

    });

});


