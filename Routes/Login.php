<?php

/*
 * SiaaS login service
 * Uses OAuth to log in through Slack itself
 */

/**
 * @var $app \SiaasSlim
 */

use Models\User;


//Check to make sure we don't already have a token in session
if($app->getOAuth()->getStorage()->hasAccessToken($app->getOAuth()->service())) {

    saveLoginAndContinue($app);

} elseif(!empty($_GET['code'])) {

    //This is a callback from slack
    $app->getOAuth()->requestAccessToken($_GET['code']);

    //Yay
    saveLoginAndContinue($app);

} else {

    //Send them on to the login page
    $url = $app->getOAuth()->getAuthorizationUri();
    $app->redirect($url);

}


/**
 * @param $app \SiaasSlim
 * @param $slackService \Models\SlackOAuthClient
 * Upsert the user and move them onto the management panel
 */
function saveLoginAndContinue($app) {

    $slackToken = $app->getOAuth()->getStorage()->retrieveAccessToken($app->getOAuth()->service())->getAccessToken();

    $stmt = $app->getDatabase()->prepare("SELECT `id` FROM `Users` WHERE `slackToken` = (?)");
    $stmt->bind_param('s', $slackToken);
    $stmt->execute();

    if($stmt->num_rows == 1) {

        //The user already exists, mark them as logged in
        $stmt->bind_result($userId);
        $_SESSION['userId'] = $userId;
        $stmt->close();

    } else {

        $stmt->close();

        //New user, request some info about them
        $result = json_decode($app->getOAuth()->request('auth.test'), true);

        //Create a new user
        $user = new User($app);
        $user->create($slackToken, $result['team_id'], $result['user_id'], $result['user']);

    }

    //Move to control panel
    $app->redirect('http://128.199.101.170/manage');

}