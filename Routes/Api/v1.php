<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 11:39 AM
 */

/*
 * Routes for V1 of the API
 */

/**
 * @var $app \SiaasSlim
 */


$fileBasePath = "Routes/Api/v1";


// API Ping Test
$app->get('/ping', function() use ($app, $fileBasePath) {

    require $fileBasePath . "/Ping.php";

});


//Users
//New user
$app->post('/user', function() use ($app, $fileBasePath) {
    require $fileBasePath . "/UserCreate.php";
});
//Get user
$app->get('/user/:id', function($id) use ($app, $fileBasePath) {
    require $fileBasePath . "/UserGet.php";
});


//Hooks (run integration)
$app->post('/hook', function() use ($app, $fileBasePath) {
    require $fileBasePath . "/Hook.php";
});




