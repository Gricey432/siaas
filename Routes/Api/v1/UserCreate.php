<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 1:45 PM
 */

/*
 * Create a new user
 */

/**
 * @var $app \SiaasSlim
 */

die('this is broken');


//Add slim mixins for JSON
$app->view(new \JsonApiView());
$app->add(new \JsonApiMiddleware());

//TODO: Make this secure

$email = $app->request()->params('email');
$name = $app->request()->params('name');
$password = $app->request()->params('password');

$user = new \Models\User($app);
$creationSuccessful = $user->create($email, $name, $password);

$data = array(
    'creationSuccessful' => $creationSuccessful,
    'id' => $user->getId(),
);

$app->render(200, $data);
