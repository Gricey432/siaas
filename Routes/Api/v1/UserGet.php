<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 1:40 PM
 */

/*
 * Returns information about a user from a given ID
 */

/**
 * @var $app \SiaasSlim
 * @var $id int
 */


//Add slim mixins for JSON
$app->view(new \JsonApiView());
$app->add(new \JsonApiMiddleware());


$user = new \Models\User($app);
$user->fetch($id);

$userData = array(
    'id' => $user->getId(),
    'name' => $user->getName(),
    'slackUserId' => $user->getSlackUserId(),
    'slackTeamId' => $user->getSlackTeamId(),
    'created' => $user->getCreated(),
);

$app->render(200, array(
    'user' => $userData,
));