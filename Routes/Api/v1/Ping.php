<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 11:42 AM
 */

/*
 * Ping function in Api v1
 *
 * Returns 'pong'
 */

/**
 * @var $app \SiaasSlim
 */


//Add slim mixins for JSON
$app->view(new \JsonApiView());
$app->add(new \JsonApiMiddleware());


$app->render(200, array(
    'ping' => 'pong',
));


