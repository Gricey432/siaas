<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 5:19 PM
 */

namespace Integrations;


interface IntegrationInterface {

    /**
     * @param $app \SiaasSlim
     * @param $request \Models\HookRequest
     * @param $options \Models\UserIntegrationValue[]
     * @return boolean
     * Perform the hook logic for the integration on new message
     */
    public function run($app, $request, $options);

}