<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 5:14 PM
 */

namespace Integrations;


class EchoIntegration extends IntegrationBase implements IntegrationInterface {

    /**
     * @param $app \SiaasSlim
     * @param $request \Models\HookRequest
     * @param $options \Models\UserIntegrationValue[]
     * @return boolean
     * Perform the hook logic for the integration on new message
     */
    public function run($app, $request, $options) {

        if($request->getUserName() == 'slackbot'){
            //Dont echo self forever
            return array();
        }

        return [
            'text' => '@' . $request->getUserName() . ': ' . $request->getText(),
            'username' => $options[0]->getValue(),
        ];

    }
}