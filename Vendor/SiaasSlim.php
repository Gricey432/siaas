<?php

/**
 * Adds some functions to Slim
 */


use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;
use OAuth\Common\Http\Client\StreamClient;
use Models\SlackOAuthClient;
use Models\User;


class SiaasSlim extends \Slim\Slim {

    protected $database;
    /** @var  $slackService \Models\SlackOAuthClient */
    protected $slackService;
    protected $siaasConfig;


    //Database carrier
    public function setDatabase($database) {
        $this->database = $database;
    }

    /**
     * @return \mysqli
     */
    public function getDatabase() {
        return $this->database;
    }


    public function setSiaasConfig($config) {
        $this->siaasConfig = $config;
    }

    public function getSiaasConfig() {
        return $this->siaasConfig;
    }


    /**
     * Init the oauth
     */
    public function initOAuth() {
        // Session storage
        $storage = new Session();


        //Setup the credentials for the requests
        $config = $this->getSiaasConfig();
        $credentials = new Credentials(
            $config['oauth']['key'],
            $config['oauth']['secret'],
            "http://128.199.101.170/login"
        );


        //Pick scopes
        $scopes = array(SlackOAuthClient::SCOPE_IDENTIFY);


        // Instantiate the Slack service using the credentials, http client and storage mechanism for the token
        /** @var $slackService \Models\SlackOAuthClient */
        $this->slackService = new SlackOAuthClient($credentials, new StreamClient(), $storage, $scopes);
    }

    /**
     * @return \Models\SlackOAuthClient
     */
    public function getOAuth() {
        return $this->slackService;
    }


    /**
     * @return boolean if the user is logged in
     * Checks if there is a user logged in
     */
    public function isLoggedIn() {
        return ($this->slackService->getStorage()->hasAccessToken($this->slackService->service()));
    }

    public function getLoggedInUser() {
        $slackToken = $this->slackService->getStorage()->retrieveAccessToken($this->slackService->service())->getAccessToken();

        $stmt = $this->getDatabase()->prepare("SELECT `id` FROM `Users` WHERE `slackToken` = (?)");
        $stmt->bind_param('s', $slackToken);
        $stmt->execute();

        $stmt->store_result();

        if($stmt->num_rows == 1) {

            //The user already exists, mark them as logged in
            $stmt->bind_result($userId);

            $stmt->fetch();
            $user = new User($this);
            $user->fetch($userId);
            $stmt->close();

            return $user;

        } else {

            $stmt->close();
            throw new Exception('Not logged in, should use isLoggedIn first. Go to homepage and click login');

        }
    }

}