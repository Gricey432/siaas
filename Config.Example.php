<?php

/*
 * Fill me out
 * Rename me to Config.php
 */

// Database
$config['db']['address'] = 'localhost';
$config['db']['port'] = 3306;
$config['db']['username'] = '';
$config['db']['password'] = '';
$config['db']['database'] = 'siaas';

// Slack OAuth
$config['oauth']['key'] = 'YOURKEYHERE';
$config['oauth']['secret'] = 'YOURSECRET';
