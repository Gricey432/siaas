<?php
/*
 * Slack Integrations as a Service
 * SIAAS
 * By Mitchell Grice
 * for the 2015 UQ Computing Society Hackathon
 */


// Debug
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ERROR | E_NOTICE | E_WARNING);


// Config
/**
 * @var $config array
 */
require_once "Config.php";


// Require autoloaders
require_once "Vendor/autoload.php";
require_once "autoload.php";


// Create a new Slim App
$app = new SiaasSlim(array(

));


//Pass in config
$app->setSiaasConfig($config);


//Set up OAuth
$app->initOAuth();


//Create database
$app->setDatabase(new mysqli($config['db']['address'], $config['db']['username'], $config['db']['password'], $config['db']['database']));
if(!$app->getDatabase()) {
    die('Database connection failed');
}


// Load routes
require "Routes/Routes.php";


// Execute the app
$app->run();

