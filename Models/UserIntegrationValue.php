<?php
/**
 * Created by PhpStorm.
 * User: garry
 * Date: 8/08/2015
 * Time: 7:20 PM
 */

namespace Models;


use Models\ORM\Field;

class UserIntegrationValue extends ORM\ORMModel {

    protected $userIntegrationId;
    protected $integrationValueId;
    protected $value;
    protected $lastUpdated;


    public function __construct($app) {
        parent::__construct($app, 'UserIntegrationValues');

        //Setup fields
        $this->userIntegrationId = new Field('userIntegrationId', Field::INT);
        $this->integrationValueId = new Field('integrationValueId', Field::INT);
        $this->value = new Field('value', Field::STRING);
        $this->lastUpdated = new Field('lastUpdated', Field::TIMESTAMP);

    }


    //Create
    public function create($userIntegrationId, $integrationValueId, $value) {
        $stmt = $this->app->getDatabase()->prepare("INSERT INTO `UserIntegrationValues` (`userIntegrationId`, `integrationValueId`, `value`) VALUES (?, ?, ?)");
        if(!$stmt) {
            die('error: ' . $this->app->getDatabase()->error);
        }
        $stmt->bind_param("iis", $userIntegrationId, $integrationValueId, $value);

        return parent::create($stmt);
    }


    //Getters
    /**
     * @return mixed
     */
    public function getUserIntegrationId()
    {
        return $this->userIntegrationId->getValue();
    }

    /**
     * @return mixed
     */
    public function getIntegrationValueId()
    {
        return $this->integrationValueId->getValue();
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value->getValue();
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated->getValue();
    }

}