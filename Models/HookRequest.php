<?php
/**
 * Created by PhpStorm.
 * User: garry
 * Date: 8/08/2015
 * Time: 7:04 PM
 */

namespace Models;

/**
 * Class HookRequest
 * @package Models
 * Turns a Slack outbound webhook request into a nice object
 */
class HookRequest {


    protected $token;
    protected $teamId;
    protected $teamDomain;
    protected $channelId;
    protected $channelName;
    protected $timestamp;
    protected $userId;
    protected $userName;
    protected $text;
    protected $triggerWord;


    /**
     * @param $app \SiaasSlim
     *
     */
    public function __construct($app) {
        /*
         * Data is of format:
         *
         * token=XXXXXXXXXXXXXXXXXX
            team_id=T0001
            team_domain=example
            channel_id=C2147483705
            channel_name=test
            timestamp=1355517523.000005
            user_id=U2147483697
            user_name=Steve
            text=googlebot: What is the air-speed velocity of an unladen swallow?
            trigger_word=googlebot:
         */

        $request = $app->request();

        $this->token = $request->params('token');
        $this->teamId = $request->params('team_id');
        $this->teamDomain = $request->params('team_domain');
        $this->channelId = $request->params('channel_id');
        $this->channelName = $request->params('channel_name');
        $this->timestamp = $request->params('timestamp');
        $this->userId = $request->params('user_id');
        $this->userName = $request->params('user_name');
        $this->text = $request->params('text');
        $this->triggerWord = $request->params('trigger_word');

    }

    /**
     * @return array|mixed|null
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return array|mixed|null
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @return array|mixed|null
     */
    public function getTeamDomain()
    {
        return $this->teamDomain;
    }

    /**
     * @return array|mixed|null
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @return array|mixed|null
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * @return array|mixed|null
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return array|mixed|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return array|mixed|null
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return array|mixed|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return array|mixed|null
     */
    public function getTriggerWord()
    {
        return $this->triggerWord;
    }



}