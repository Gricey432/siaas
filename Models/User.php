<?php
namespace Models;
use Models\ORM\Field;

/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 12:05 PM
 */

class User extends ORM\ORMModel {

    protected $slackToken;
    protected $slackTeamId;
    protected $slackUserId;
    protected $name;
    protected $created;


    public function __construct($app) {
        parent::__construct($app, 'Users');

        //Create the object
        $this->slackToken = new Field('slackToken', Field::STRING);
        $this->slackTeamId = new Field('slackTeamId', Field::STRING);
        $this->slackUserId = new Field('slackUserId', Field::STRING);
        $this->name = new Field('name', Field::STRING);
        $this->created = new Field('created', Field::TIMESTAMP);
    }


    // Create
    /**
     * @param $slackToken string
     * @param $slackTeamId string
     * @param $slackUserId string
     * @param $name string
     * @return bool
     */
    public function create($slackToken, $slackTeamId, $slackUserId, $name) {
        $stmt = $this->app->getDatabase()->prepare("INSERT INTO `Users` (`slackToken`, `slackTeamId`, `slackUserId`, `name`) VALUES (?, ?, ?, ?)");
        if(!$stmt) {
            die('error: ' . $this->app->getDatabase()->error);
        }
        $stmt->bind_param("ssss", $slackToken, $slackTeamId, $slackUserId, $name);

        return parent::create($stmt);
    }


    /**
     * @return \Models\UserIntegration[]
     * Get all integrations associated with this user
     */
    public function getUserIntegrations() {
        $qry = $this->app->getDatabase()->query("SELECT `id` FROM UserIntegrations WHERE `UserId` = " . $this->getId());

        $data = array();
        while($row = $qry->fetch_assoc()) {
            $userIntegration = new UserIntegration($this->app);
            $userIntegration->fetch($row['id']);
            array_push($data, $userIntegration);
        }
        return $data;
    }


    //Getters
    /**
     * @return Field
     */
    public function getSlackToken()
    {
        return $this->slackToken->getValue();
    }

    /**
     * @return Field
     */
    public function getSlackTeamId()
    {
        return $this->slackTeamId->getValue();
    }

    /**
     * @return Field
     */
    public function getSlackUserId()
    {
        return $this->slackUserId->getValue();
    }

    /**
     * @return Field
     */
    public function getName()
    {
        return $this->name->getValue();
    }

    /**
     * @return Field
     */
    public function getCreated()
    {
        return $this->created->getValue();
    }

}