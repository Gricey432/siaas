<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 9/08/2015
 * Time: 3:20 PM
 */

namespace Models;


use Models\ORM\Field;

class UserIntegration extends ORM\ORMModel {

    protected $userId;
    protected $integrationId;
    protected $enabled;
    protected $slackToken;
    protected $created;


    public function __construct($app) {
        parent::__construct($app, 'UserIntegrations');

        //Create object
        $this->userId = new Field('userId', Field::INT);
        $this->integrationId = new Field('integrationId', Field::INT);
        $this->enabled = new Field('enabled', Field::BOOLEAN);
        $this->slackToken = new Field('slackToken', Field::STRING);
        $this->created = new Field('created', Field::TIMESTAMP);
    }



    public function create($userId, $integrationId, $enabled, $slackToken) {
        $stmt = $this->app->getDatabase()->prepare("INSERT INTO `UserIntegrations` (`userId`, `integrationId`, `enabled`, `slackToken`) VALUES(?, ?, ?, ?)");
        if(!$stmt) {
            die('error: ' . $this->app->getDatabase()->error);
        }
        $enabledInt = $enabled ? 1 : 0;
        $stmt->bind_param('iiis', $userId, $integrationId, $enabledInt, $slackToken);

        return parent::create($stmt);
    }


    public function getUserIntegrationValues() {

        $qry = $this->app->getDatabase()->query("SELECT `id` FROM `UserIntegrationValues` WHERE `userIntegrationId` = " . $this->getId());

        $data = array();
        while($row = $qry->fetch_assoc()) {
            $userIntegrationValue = new UserIntegrationValue($this->app);
            $userIntegrationValue->fetch($row['id']);
            array_push($data, $userIntegrationValue);
        }

        return $data;
    }


    //GETTERS
    public function getUserId()
    {
        return $this->userId->getValue();
    }
    public function getIntegrationId()
    {
        return $this->integrationId->getValue();
    }
    public function getEnabled()
    {
        return $this->enabled->getValue();
    }
    public function getSlackToken()
    {
        return $this->slackToken->getValue();
    }
    public function getCreated()
    {
        return $this->created->getValue();
    }

}