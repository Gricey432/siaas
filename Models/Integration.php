<?php
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 4:33 PM
 */

namespace Models;


use Models\ORM\Field;

class Integration extends ORM\ORMModel {

    protected $userId;
    protected $name;
    protected $description;
    protected $phpClass;
    protected $created;


    public function __construct($app) {
        parent::__construct($app, 'Integrations');

        //Create the object
        $this->userId = new Field('userId', Field::INT);
        $this->name = new Field('name', Field::STRING);
        $this->description = new Field('description', Field::STRING);
        $this->phpClass = new Field('phpClass', Field::STRING);
        $this->created = new Field('created', Field::TIMESTAMP);
    }

    /**
     * @return IntegrationValue[]
     */
    public function getIntegrationValues() {

        $qry = $this->app->getDatabase()->query("SELECT `id` FROM `IntegrationValues` WHERE `integrationId` = " . $this->getId());

        $data = array();
        while($row = $qry->fetch_assoc()) {
            $integrationValue = new IntegrationValue($this->app);
            $integrationValue->fetch($row['id']);
            array_push($data, $integrationValue);
        }

        return $data;
    }


    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId->getValue();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name->getValue();
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description->getValue();
    }

    public function getPhpClass() {
        return $this->phpClass->getValue();
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created->getValue();
    }


}
