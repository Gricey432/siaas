<?php

namespace Models;


use Models\ORM\Field;


class IntegrationValue extends ORM\ORMModel {

    protected $integrationId;
    protected $name;
    protected $default;
    protected $required;
    protected $hidden;


    public function __construct($app) {
        parent::__construct($app, 'IntegrationValues');


        //Create the object
        $this->integrationId = new Field('integrationId', Field::INT);
        $this->name = new Field('name', Field::STRING);
        $this->default = new Field('default', Field::STRING);
        $this->required = new Field('required', Field::BOOLEAN);
        $this->hidden = new Field('hidden', Field::BOOLEAN);
    }




    /**
     * @return mixed
     */
    public function getIntegrationId()
    {
        return $this->integrationId->getValue();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name->getValue();
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default->getValue();
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required->getValue();
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden->getValue();
    }

}