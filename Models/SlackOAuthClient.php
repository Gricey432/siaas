<?php
/**
 * Created by PhpStorm.
 * User: garry
 * Date: 8/08/2015
 * Time: 9:35 PM
 */

namespace Models;


use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Http\Uri\UriInterface;
use OAuth\OAuth2\Service\AbstractService;
use OAuth\Common\Http\Uri\Uri;
use OAuth\Common\Consumer\CredentialsInterface;
use OAuth\Common\Http\Client\ClientInterface;
use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\OAuth2\Token\StdOAuth2Token;


class SlackOAuthClient Extends AbstractService {

    /**
     * Scopes
     *
     * @var string
     */
    const SCOPE_IDENTIFY = "identify";
    const SCOPE_READ = "read";
    const SCOPE_POST = "post";
    const SCOPE_CLIENT = "client";
    const SCOPE_ADMIN = "admin";




    public function __construct(
        CredentialsInterface $credentials,
        ClientInterface $httpClient,
        TokenStorageInterface $storage,
        $scopes = array(),
        UriInterface $baseApiUri = null
    ) {
        parent::__construct($credentials, $httpClient, $storage, $scopes, $baseApiUri, true);
        if (null === $baseApiUri) {
            $this->baseApiUri = new Uri('https://slack.com/api/');
        }
    }


    /**
     * {@inheritdoc}
     */
    protected function parseAccessTokenResponse($responseBody)
    {
        $data = json_decode($responseBody, true);

        if($data === null || !is_array($data)) {
            throw new TokenResponseException('Unable to parse response');
        } elseif (isset($data['error'])) {
            throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
        }


        $token = new StdOAuth2Token();
        print_r($data);
        $token->setAccessToken($data['access_token']);

        // Slack access tokens don't expire

        unset($data['access_token']);

        $token->setExtraParams($data);

        return $token;

    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationEndpoint()
    {
        return new Uri("https://slack.com/oauth/authorize");
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessTokenEndpoint()
    {
        return new Uri("https://slack.com/api/oauth.access");
    }

    /**
     * {@inheritdoc}
     */
    protected function getAuthorizationMethod()
    {
        return static::AUTHORIZATION_METHOD_QUERY_STRING_V5;
    }

}