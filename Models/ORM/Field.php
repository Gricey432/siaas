<?php
namespace Models\ORM;
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 12:15 PM
 */


/**
 * Class Field
 * @package Models\ORM
 * Represents a database field
 */
class Field {

    const INT = "int";
    const STRING = "string";
    const TIMESTAMP = "timestamp";
    const BOOLEAN = "boolean";


    protected $field;
    protected $type;
    protected $value = null;


    public function __construct($field, $type) {
        $this->field = $field;
        $this->type = $type;
    }

    /* Getters */
    public function getField() {
        return $this->field;
    }
    public function getType() {
        return $this->type;
    }
    public function getValue() {
        return $this->value;
    }

    /* Setters */
    /**
     * @param $newValue mixed
     * @param $saveToDatabase boolean Flush value to database
     * Updates the value and updates the database
     */
    public function setValue($newValue, $saveToDatabase = true) {
        $this->value = $newValue;

        if($saveToDatabase) {

            // Call the model to update itself


        }

    }

}