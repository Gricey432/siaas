<?php
namespace Models\ORM;
/**
 * Created by PhpStorm.
 * User: Mitchell
 * Date: 8/08/2015
 * Time: 11:45 AM
 */

/**
 * Class ORMModel
 * @package Models
 * Some sort of awful madeup ORM for hackathon
 * All tables have a unique field named 'id' which is an unsigned AI int
 */
class ORMModel {

    /**
     * @var $app \SiaasSlim
     */
    protected $app;
    protected $table;
    protected $id;

    public function __construct($app, $table) {
        $this->app = $app;
        $this->table = $table;
    }

    /**
     * @param $id int Row ID in db
     * Fetches the object data from this ID
     */
    public function fetch($id) {
        $this->id = $id;
        $this->select();
    }


    public function getId() {
        return $this->id;
    }


    protected function setTable($table) {
        $this->table = $table;
    }


    protected function setField($field, $value) {
        $this->update($field, $value);
    }

    /**
     * @param $stmt \mysqli_stmt
     * @return bool
     */
    protected function create($stmt) {
        //Execute
        $executed = $stmt->execute();
        if($executed) {
            $this->id = $stmt->insert_id;
            $this->select();
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }

    }

    protected function select() {
        //Build query
        $query = $this->app->getDatabase()->query("SELECT * FROM `" . $this->table . "` WHERE `id` = '" . $this->id . "'");

        //TODO: Make this into a prepared statement somehow

        //Fetch data
        $res = $query->fetch_assoc();

        //Update the object with this new data
        foreach($res as $key => $val) {

            //Ignore ID
            if($key == 'id')
                continue;

            // Set the value
            $this->{$key}->setValue($val, false);

        }

        return true;
        //TODO: add some sort of failure detection to this
    }

}